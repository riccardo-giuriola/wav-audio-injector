﻿using System;
using System.Linq;
using System.Windows.Forms;
using NAudio.Wave;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Text.RegularExpressions;
using VideoLibrary;
using NReco.VideoConverter;
using MediaToolkit.Model;
using MediaToolkit;
using NAudio.Wave.SampleProviders;

namespace WAI
{
    public partial class Form1 : Form
    {
        public static String key = "x";
        static String fullpath = "";
        static String tracksDirectory = Directory.GetCurrentDirectory() + "\\tracks";
        static String trackNumber = "";
        static String trackNumberString = "";
        static String firstSubDir = "";
        static String[] track;
        static String[] trackLenght;
        static String YoutubeName = "";
        static bool running = true;
        static bool change = false;
        static Thread thread;
        static String youtubename = "";

        public Form1()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void updateListView()
        {
            getFileLenght();
            DirectoryInfo dinfo = new DirectoryInfo("tracks\\");
            FileInfo[] Files = dinfo.GetFiles("*.wav");
            int i = 1;
            int trackId = 0;
            listView1.Items.Clear();
            foreach (FileInfo file in Files)
            {
                ListViewItem lv1 = new ListViewItem();
                lv1.SubItems.Add(i.ToString());
                lv1.SubItems.Add(System.IO.Path.GetFileNameWithoutExtension(file.Name));
                lv1.SubItems.Add(trackLenght[trackId]);
                listView1.Items.Add(lv1);
                i++;
                trackId++;
            }
        }

        private void getFileLenght()
        {
            DirectoryInfo dinfo = new DirectoryInfo("tracks\\");
            FileInfo[] Files = dinfo.GetFiles("*.wav");
            DirectoryInfo dir = new DirectoryInfo(Directory.GetCurrentDirectory() + "\\tracks\\");
            int count = dir.GetFiles().Length;
            int i = 0;
            trackLenght = new string[count];
            foreach (FileInfo file in Files)
            {
                using (var time = new WaveFileReader(dir + file.Name))
                {
                    trackLenght[i] = (time.TotalTime).ToString();
                }
                i++;
            }
        }

        private void btn_import_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "MP3 File (*.mp3)|*.mp3;";
            open.Multiselect = true;
            if (open.ShowDialog() != DialogResult.OK) return;

            foreach (String file in open.FileNames)
            {
                using (Mp3FileReader mp3 = new Mp3FileReader(file))
                {
                    using (WaveStream pcm = WaveFormatConversionStream.CreatePcmStream(mp3))
                    {
                        WaveFileWriter.CreateWaveFile("conversion.wav", pcm);

                        WaveFormat target = new WaveFormat(23000, 16, 1);
                        WaveStream stream = new WaveFileReader("conversion.wav");
                        WaveFormatConversionStream str = new WaveFormatConversionStream(target, stream);
                        WaveFileWriter.CreateWaveFile("tracks\\" + System.IO.Path.GetFileNameWithoutExtension(file) + ".wav", str);
                        stream.Close();
                        str.Dispose();
                        listView1.Items.Clear();
                    }
                    updateListView();
                    System.IO.File.Delete("conversion.wav");
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\tracks");

            Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\temp");

            updateListView();
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            foreach (Process PPath in Process.GetProcessesByName("csgo"))
            {
                fullpath = PPath.MainModule.FileName;
            }

            if (btn_start.Text == "Start")
            {
                if (fullpath == "")
                {
                    running = false;
                }
                else
                {
                    btn_import.Enabled = false;
                    btn_youtube.Enabled = false;
                    btn_info.Enabled = false;
                    btn_start.Text = "Stop";
                    running = true;
                }
            }
            else if (btn_start.Text == "Stop")
            {
                btn_import.Enabled = true;
                btn_youtube.Enabled = true;
                btn_info.Enabled = true;
                btn_start.Text = "Start";
                running = false;
            }

            if (fullpath == "")
            {
                MessageBox.Show("Start CS:GO and try again");
            }
            else
            {

                fullpath = fullpath.Remove(fullpath.IndexOf("csgo.exe"), "csgo.exe".Length);

                if (File.Exists(fullpath + "\\csgo\\cfg\\wai.cfg"))
                {
                    File.Delete(fullpath + "\\csgo\\cfg\\wai.cfg");
                }

                DirectoryInfo dir = new DirectoryInfo(Directory.GetCurrentDirectory() + "\\tracks");
                int count = dir.GetFiles().Length;

                TextWriter tw = new StreamWriter(fullpath + "\\csgo\\cfg\\wai.cfg", true);
                tw.WriteLine("alias list \"exec wai_tracklist.cfg\"");
                tw.WriteLine("alias key_pressed music_on");
                tw.WriteLine("alias music_on \"alias key_pressed music_off; voice_inputfromfile 1; voice_loopback 1; +voicerecord\"");
                tw.WriteLine("alias music_off \"-voicerecord; voice_inputfromfile 0; voice_loopback 0; alias key_pressed music_on\"");
                tw.WriteLine("alias wai_updatecfg \"host_writeconfig wai_relay\"");
                tw.WriteLine("bind " + key + " key_pressed");
                tw.WriteLine("echo \"Commands loaded, just press " + key + " to start / stop playing your song.\"");

                DirectoryInfo d = new DirectoryInfo(Directory.GetCurrentDirectory() + "\\tracks");
                FileInfo[] Files = d.GetFiles("*.wav");
                track = new string[count];
                int place = 0;
                foreach (FileInfo file in Files)
                {
                    track[place] = file.Name;
                    track[place] = track[place].Replace(".wav", "");
                    place++;
                }

                for (int i = 0, a = 1, j = 0; i < count; i++)
                {
                    tw.WriteLine("alias " + a + " \"bind = " + a + "; wai_updatecfg; echo Loaded: " + track[j] + "\"");
                    j++;
                    a++;
                }

                tw.WriteLine("voice_enable 1; voice_modenable 1; voice_forcemicrecord 0; con_enable 1");

                tw.Close();

                if (running == true)
                {
                    MessageBox.Show("Don't forget to write \"exec wai\" in CS:GO console to load the program.\nWrite 'tracks' to see your sounds list.");
                }
                else if (running == false)
                {
                    MessageBox.Show("WAI has been stopped.");
                }

                String steamPath = "";

                foreach (Process PPath in Process.GetProcessesByName("Steam"))
                {
                    steamPath = PPath.MainModule.FileName;
                }

                steamPath = steamPath.Remove(steamPath.IndexOf("Steam.exe"), "Steam.exe".Length);

                string[] subDirectories = System.IO.Directory.GetDirectories(steamPath + "\\userdata");
                if (subDirectories.Length > 0)
                {
                    firstSubDir = subDirectories[0];
                }

                if (File.Exists(fullpath + "\\csgo\\cfg\\wai_selectedtrack.cfg"))
                {
                    File.Delete(fullpath + "\\csgo\\cfg\\wai_selectedtrack.cfg");
                }

                Thread t = new Thread(findTrackNumber);
                t.Start();

                Thread t2 = new Thread(copyAudioTrack);
                t2.Start();

                if (running == false)
                {
                    t.Abort();
                    t2.Abort();
                }

                if (File.Exists(fullpath + "\\csgo\\cfg\\wai_tracklist.cfg"))
                {
                    File.Delete(fullpath + "\\csgo\\cfg\\wai_tracklist.cfg");
                }

                TextWriter tw3 = new StreamWriter(fullpath + "\\csgo\\cfg\\wai_tracklist.cfg", true);
                tw3.WriteLine("echo \"                   Track List:\"");
                tw3.WriteLine("echo \"-------------------------------------------------------------\"");
                int k = 1;
                for (int i = 0; i < count; i++)
                {
                    tw3.WriteLine("echo \"" + k + " " + track[i] + "\"");
                    k++;
                }
                tw3.WriteLine("echo \"-------------------------------------------------------------\"");
                tw3.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Work in progress.");
        }

        void ChangeKey_FormClosed(object sender, FormClosedEventArgs e)
        {
            MessageBox.Show("hello");
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (fullpath != "")
            {
                File.Delete(fullpath + "\\csgo\\cfg\\wai.cfg");
            }
        }

        static void findTrackNumber()
        {
            while (true)
            {
                if (File.Exists(firstSubDir + "\\730\\local\\cfg\\wai_relay.cfg"))
                {
                    trackNumber = File.ReadLines(firstSubDir + "\\730\\local\\cfg\\wai_relay.cfg").Skip(33).Take(1).First();
                    trackNumberString = Regex.Replace(trackNumber, "[^.0-9]", "");
                }
                else
                {

                }
                System.Threading.Thread.Sleep(10);
            }
        }

        static void copyAudioTrack()
        {
            String changedTrackNumber = "";
            while (true)
            {
                if (trackNumberString != "")
                {
                    changedTrackNumber = trackNumberString;
                    break;
                }
                System.Threading.Thread.Sleep(1);
            }
            while (true)
            {
                if (changedTrackNumber != trackNumberString)
                {
                    if (File.Exists(fullpath + "\\voice_input.wav"))
                    {
                        File.Delete(fullpath + "\\voice_input.wav");
                    }

                    File.Copy(Directory.GetCurrentDirectory() + "\\tracks\\" + track[Int32.Parse(trackNumberString) - 1] + ".wav", fullpath + "\\voice_input.wav");

                    changedTrackNumber = trackNumberString;

                }
                System.Threading.Thread.Sleep(1);
            }
        }

        private void YouTubeDownload()
        {
            var youTube = YouTube.Default; // starting point for YouTube actions
            var vid = youTube.GetVideo(txt_url.Text); // gets a Video object with info about the video
            youtubename = vid.FullName;
            File.WriteAllBytes(Directory.GetCurrentDirectory() + "\\temp\\" + youtubename, vid.GetBytes());
        }

        private void convertMp3()
        {
            var ConvertVideo = new NReco.VideoConverter.FFMpegConverter();
            ConvertVideo.ConvertMedia(Directory.GetCurrentDirectory() + "\\temp\\" + youtubename, Directory.GetCurrentDirectory() + "\\temp\\" + youtubename + ".mp3", "mp3");
        }

        private void convertWav()
        {
            //var ConvertVideo = new NReco.VideoConverter.FFMpegConverter();
            //ConvertVideo.ConvertMedia(Directory.GetCurrentDirectory() + "\\temp\\" + "mp3Conversion.mp3", Directory.GetCurrentDirectory() + "\\temp\\" + "ciao.wav", "wav");

            //var convert = new FFMpegConverter();
            //convert.ExtractFFmpeg();

            //var command = String.Format("-i {0} -n -f wav -flags bitexact -map_metadata -1 -vn -acodec pcm_s16le -ar {1} -ac {2} {3}", Directory.GetCurrentDirectory() + "\\temp\\" + "ciao.wav", 22050, 1, Directory.GetCurrentDirectory() + "\\tracks\\" + "ciao.wav");
            //convert.Invoke(command);

            //var reader = new MediaFoundationReader(Directory.GetCurrentDirectory() + "\\temp\\" + "mp3Conversion.mp3");

            //var outFormat = new WaveFormat(22050, 16, 1);

            //var resampler = new MediaFoundationResampler(reader, outFormat);

            //resampler.ResamplerQuality = 60;

            //WaveFileWriter.CreateWaveFile(Directory.GetCurrentDirectory() + "\\tracks\\" + youtubename + ".wav", resampler);

            //resampler.Dispose();

            using (Mp3FileReader reader = new Mp3FileReader(Directory.GetCurrentDirectory() + "\\temp\\" + youtubename + ".mp3"))
            {
                var newFormat = new WaveFormat(22050, 16, 1);
                using (var conversionStream = new WaveFormatConversionStream(newFormat, reader))
                {
                    WaveFileWriter.CreateWaveFile(Directory.GetCurrentDirectory() + "\\tracks\\" + youtubename + ".wav", conversionStream);
                }
            }
        }

        private void btn_youtube_Click(object sender, EventArgs e)
        {

            if (txt_url.Text.Equals(""))
            {
                MessageBox.Show("Invalid URL.");
            }
            else
            {
                YouTubeDownload();
                convertMp3();
                convertWav();
                updateListView();

                if (File.Exists(Directory.GetCurrentDirectory() + "\\temp\\" + youtubename + " - YouTube.mp4"))
                {
                    File.Delete(Directory.GetCurrentDirectory() + "\\temp\\" + youtubename + " - YouTube.mp4");
                }
                if (File.Exists(Directory.GetCurrentDirectory() + "\\temp\\" + youtubename + " - YouTube.webm"))
                {
                    File.Delete(Directory.GetCurrentDirectory() + "\\temp\\" + youtubename + " - YouTube.webm");
                }
                if (File.Exists(Directory.GetCurrentDirectory() + "\\temp\\aac.mp4"))
                {
                    File.Delete(Directory.GetCurrentDirectory() + "\\temp\\aac.mp4");
                }

            }
        }

        private void btn_info_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Work in progress.");
        }
    }
}
