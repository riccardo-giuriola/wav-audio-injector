﻿namespace WAI
{
    partial class Alert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_alert = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt_alert
            // 
            this.txt_alert.Location = new System.Drawing.Point(12, 9);
            this.txt_alert.Name = "txt_alert";
            this.txt_alert.Size = new System.Drawing.Size(360, 93);
            this.txt_alert.TabIndex = 0;
            this.txt_alert.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Alert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 111);
            this.Controls.Add(this.txt_alert);
            this.Name = "Alert";
            this.Text = "Alert";
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label txt_alert;
    }
}